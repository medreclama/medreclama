import flexslider from 'flexslider'; // eslint-disable-line
import circles from 'circles'; // eslint-disable-line
import countdown from './jquery.countdown'; // eslint-disable-line

import headerMobileMenu from '../blocks/header-mobile/header-mobile-menu/header-mobile-menu';
import circlesDraw from '../blocks/figure-diagram/figure-diagram';
import resultsCarousel from '../blocks/results-carousel/results-carousel';
import countdownBlock from '../blocks/countdown/countdown';
import caseCtrRound from '../blocks/case/case-ctr-round/case-ctr-round';
import feedbackList from '../blocks/feedback-list/feedback-list';
import bookModule from '../blocks/book-module/book-module';
import bookModuleMobile from '../blocks/book-module-mobile/book-module-mobile';
import formWebinar from '../blocks/form-webinar/form-webinar';
import blogContents from '../blocks/blog-contents/blog-contents';
import portfolioButtons from '../blocks/portfolio-buttons/portfolio-buttons';
import modal from '../blocks/modal/modal';
import Form from './form/Form';
import homeProcess from '../blocks/home-process/home-process';
import stepsScrollInit from '../blocks/steps-scroll/steps-scroll';
import IntersectionObserverClass from '../blocks/intersection-observer/intersection-observer';
import counters from './modules/counters';
import mrcSH from './modules/show-hide';
import youtubeLoadingLazy from '../blocks/youtube-responsive/youtube-responsive';
import blogInteresting from '../blocks/blog-interesting/blog-interesting';
// import socialLock from '../blocks/social-lock/social-lock';
// import circles from '../../node_modules/circles/circles.js';

headerMobileMenu();
circlesDraw();
resultsCarousel();
countdownBlock();
caseCtrRound();
feedbackList();
bookModule();
bookModuleMobile();
formWebinar();
blogContents();
portfolioButtons();
modal();
homeProcess();
stepsScrollInit();
mrcSH();
blogInteresting();
// socialLock();

const countersCodeHead = `
  <script type="text/javascript">
    !function(){
      var t=document.createElement("script");
      t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?157",t.onload=function(){
        VK.Retargeting.Init("VK-RTRG-262191-i0HBE"),VK.Retargeting.Hit()
      },document.head.appendChild(t)
    }();
  </script>
  <noscript>
    <img src="https://vk.com/rtrg?p=VK-RTRG-262191-i0HBE" style="position:fixed; left:-999px;" alt=""/>
  </noscript>
  <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-35671688-3']);
    _gaq.push(['_trackPageview']);
    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
  </script>
`;

const countersCodeBody = `
  <!-- Yandex.Metrika counter -->
  <script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
    m[i].l=1*new Date();
    for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
    k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(24832055, "init", {
          clickmap:true,
          trackLinks:true,
          accurateTrackBounce:true,
          webvisor:true
    });
  </script>
  <noscript><div><img src="https://mc.yandex.ru/watch/24832055" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
  <!-- /Yandex.Metrika counter -->
`;

window.addEventListener('DOMContentLoaded', () => {
  counters(countersCodeBody, 'body');
  counters(countersCodeHead, 'head');
  // counters('<script id="form-5ec687899fe1864f9d000262" type="text/javascript" src="https://api.siter.justclick.ru/web_forms/5ec687899fe1864f9d000262/form_loader.js?cid=form-5ec687899fe1864f9d000262"></script>', 'book-module__title');
  // counters('<script id="form-5ec687899fe1864f9d000262" type="text/javascript" src="https://api.siter.justclick.ru/web_forms/5ec687899fe1864f9d000262/form_loader.js?cid=form-5ec687899fe1864f9d000262"></script>', '.book-module-mobile__title', 'body');

  const headerLink = document.querySelector('.header-contacts__link[href="#request"]');
  if (headerLink) {
    headerLink.addEventListener('click', (evt) => {
      evt.preventDefault();
      const blockID = headerLink.getAttribute('href').substring(1);
      document.getElementById(`${blockID}`).scrollIntoView({ block: 'start' });
    });
  }

  youtubeLoadingLazy();

  const vk = document.getElementById('vk_groups');
  const youtubeModule = document.querySelector('.youtube-module');

  const createScript = (src) => {
    const script = document.createElement('script');
    script.setAttribute('src', src);
    return script;
  };

  const createScriptWithContent = (content) => {
    const script = document.createElement('script');
    script.innerHTML = content;
    return script;
  };

  const youtubeModuleIntersectionObserver = new IntersectionObserverClass(youtubeModule, () => {
    const youtubeScript = createScript('https://apis.google.com/js/platform.js');
    youtubeModule.after(youtubeScript);
  });

  const vkIntersectionObserver = new IntersectionObserverClass(vk, () => {
    const vkScript1 = createScript('//vk.com/js/api/openapi.js?151');
    const vkScript2 = createScriptWithContent('VK.Widgets.Group("vk_groups", {mode: 3, width: 304, height: 205}, 86134177);');
    vk.before(vkScript1);
    vk.after(vkScript2);
  });

  youtubeModuleIntersectionObserver.init();
  vkIntersectionObserver.init();

  $('.home-feedback__slides').flexslider({
    animation: 'slide',
    controlNav: false,
    prevText: '',
    nextText: '',
    slideshow: false,
  });
  $('.result-slider').flexslider({
    animation: 'slide',
    controlNav: true,
    prevText: '',
    nextText: '',
    slideshow: false,
  });
  $('.case-hairdesign.slider').flexslider({
    animation: 'slide',
    controlNav: true,
    prevText: '',
    nextText: '',
    slideshow: false,
  });
  $('.slider.slider--development-cases').flexslider({
    animation: 'slide',
    controlNav: true,
    prevText: '',
    nextText: '',
    slideshow: false,
  });
});

const homeCases = () => {
  const getGridSize = () => ((window.innerWidth < 800) ? 1 : 2);
  window.addEventListener('DOMContentLoaded', () => {
    $('.home-cases.slider').flexslider({
      animation: 'slide',
      controlNav: true,
      prevText: '',
      itemWidth: 550,
      itemMargin: 40,
      nextText: '',
      slideshow: false,
      minItems: getGridSize(),
      maxItems: getGridSize(),
    });
  });
};
homeCases();

const threeCardsSlider = () => {
  const getGridSize = () => ((window.innerWidth < 800) ? 2 : 3);
  window.addEventListener('DOMContentLoaded', () => {
    $('.three-cards.slider').flexslider({
      animation: 'slide',
      controlNav: true,
      prevText: '',
      itemWidth: 300,
      itemMargin: 30,
      nextText: '',
      slideshow: false,
      minItems: getGridSize(),
      maxItems: getGridSize(),
    });
  });
};
threeCardsSlider();

document.querySelectorAll('form.form-ajax').forEach((form) => new Form(form));

document.querySelector('.header-logo a').addEventListener('mousedown', (e) => {
  if (e.ctrlKey) {
    e.target.href = '/admin';
  }
  setTimeout(() => { e.target.href = '/'; }, 2000);
});
