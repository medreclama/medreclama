// import jquery from 'jquery';
import flexslider from 'flexslider'; // eslint-disable-line

export default function resultsCarousel() {
  window.addEventListener('load', () => {
    $('#results-carousel__content').flexslider({
      animation: 'slide',
      directionNav: false,
      // controlNav: false,
      animationLoop: false,
      slideshow: false,
      sync: '#results-carousel__tabs',
    });

    $('#results-carousel__tabs').flexslider({
      animation: 'slide',
      controlNav: false,
      directionNav: false,
      animationLoop: false,
      slideshow: false,
      asNavFor: '#results-carousel__content',
      direction: 'vertical',
      minItems: 100,
    });

    const carouselTabIndexes = document.querySelectorAll('.results-carousel__tab-label');
    let carouselTabCount = 0;
    carouselTabIndexes.forEach((item) => {
      if (carouselTabCount === 0) {
        item.classList.add('results-carousel__tab-label--active');
      }
      carouselTabCount += 1;
      item.addEventListener('click', () => {
        carouselTabIndexes.forEach((i) => i.classList.remove('results-carousel__tab-label--active'));
        item.classList.add('results-carousel__tab-label--active');
      });
    });
  });
}
