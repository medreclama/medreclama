export default class IntersectionObserverClass {
  #element;
  #fn;
  #observer;

  constructor(element, fn) {
    this.#element = element;
    this.#fn = fn;
  }

  #processEntry = (entry) => {
    if (entry.isIntersecting) {
      this.#fn();
      this.#observer.unobserve(entry.target);
    }
  };

  #handleIntersection = (entries) => {
    entries.forEach(this.#processEntry);
  };

  init = () => {
    if (this.#element) {
      this.#observer = new IntersectionObserver((this.#handleIntersection), {
        rootMargin: '50px 0px 0px',
      });
      this.#observer.observe(this.#element);
    }
  };
}
