const getActiveButton = (currentButton) => {
  document.querySelector('.btn--active').classList.remove('btn--active');
  currentButton.classList.add('btn--active');
};

const portfolioButtons = () => {
  const portfolio = document.querySelector('.portfolio-list');
  const buttonContainer = document.querySelector('.portfolio-buttons');
  if (!buttonContainer) return;
  const categories = portfolio.querySelectorAll('[data-category]');
  const message = document.createElement('div');
  message.innerHTML = 'Ничего не найдено';

  buttonContainer.addEventListener('click', (evt) => {
    if (evt.target.tagName === 'BUTTON') {
      getActiveButton(evt.target);
      message.remove();
      categories.forEach((category) => {
        const categoryText = category.innerHTML;
        const categoryItem = category.closest('a');
        if (evt.target.dataset.button === 'Все' || evt.target.dataset.button === categoryText) portfolio.append(categoryItem);
        else categoryItem.remove();
      });
      if (portfolio.querySelectorAll('[data-category]').length === 0) {
        portfolio.before(message);
      }
    }
  });
};

export default portfolioButtons;
