export default function countdownBlock() {
  window.addEventListener('load', () => {
    const countdowns = document.querySelectorAll('.countdown');
    countdowns.forEach((item) => {
      const { year } = item.dataset;
      const { month } = item.dataset;
      const { day } = item.dataset;
      $(item).countdown({ timestamp: new Date(year, month, day, 23, 59, 59) });
    });
  });
}
