import circles from 'circles';

export default function caseCtrRound() {
  const circlesList = document.querySelectorAll('.case-ctr-round__diagram');
  const circlesSettings = {
    radius: 70,
    maxValue: 100,
    width: 30,
    text(value) { return `CTR<br><strong>${value}%</strong>`; },
    colors: ['#cccccc', '#8c3052'],
    duration: 400,
    wrpClass: 'case-ctr-round__wrap',
    textClass: 'case-ctr-round__text',
    valueStrokeClass: 'case-ctr-round__valueStroke',
    maxValueStrokeClass: 'case-ctr-round__maxValueStroke',
    styleWrapper: true,
    styleText: false,
  };
  circlesList.forEach((item) => {
    circlesSettings.id = item.id;
    circlesSettings.value = item.dataset.count;
    circles.create(circlesSettings);
  });
}
