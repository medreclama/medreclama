const blogInteresting = () => {
  const currentPath = window.location.pathname;

  if (currentPath.startsWith('/blog/page-')) {
    const blockToHide = document.querySelector('.blog-interesting');
    blockToHide.closest('.content_under').style.display = 'none';
  }
};

export default blogInteresting;
