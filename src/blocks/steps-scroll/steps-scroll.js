const stepsScroll = document.querySelector('.steps-scroll');

const onStepsScrolling = () => {
  if (!stepsScroll) return;
  const links = stepsScroll.querySelectorAll('a[href*="#"]');
  links.forEach((link) => {
    link.addEventListener('click', (evt) => {
      evt.preventDefault();
      const blockID = link.getAttribute('href').substring(1);
      document.getElementById(`${blockID}`).scrollIntoView({ block: 'start' });
    });
  });

  const steps = stepsScroll.querySelectorAll('.steps-scroll__step');
  const navItems = stepsScroll.querySelectorAll('.steps-scroll__nav-item');
  const navAvancements = stepsScroll.querySelectorAll('.steps-scroll__avancement');

  steps.forEach((step) => {
    const top = window.scrollY;
    const offset = step.offsetTop - 15; // 15px - поправка
    navItems.forEach((navItem) => {
      if (top >= offset && step.id === navItem.dataset.stepNumber) navItem.classList.add('steps-scroll__nav-item--active');
      else if (top < offset && step.id === navItem.dataset.stepNumber) navItem.classList.remove('steps-scroll__nav-item--active');
    });
    const percentage = Math.round(((window.scrollY - step.offsetTop) / step.scrollHeight) * 100);
    navAvancements.forEach((navAvancement) => {
      const avancement = navAvancement;
      // eslint-disable-next-line max-len
      if (percentage > 0 && percentage <= 120 && step.id === avancement.dataset.avancementNumber) {
        avancement.style.height = `${percentage}%`;
      }
    });
  });
};

const stepsScrollInit = () => {
  if (!stepsScroll) return;
  const steps = stepsScroll.querySelectorAll('.steps-scroll__step');
  const navAvancements = stepsScroll.querySelectorAll('.steps-scroll__avancement');

  // Событие на скролл
  document.addEventListener('scroll', onStepsScrolling);

  // Событие после перезагрузки страницы
  const observer = new IntersectionObserver((entries) => {
    entries.forEach((entry) => {
      if (!entry.isIntersecting) {
        onStepsScrolling();
        navAvancements.forEach((navAvancement) => {
          const avancement = navAvancement;
          if (avancement.closest('.steps-scroll__nav-item--active') && avancement.dataset.avancementNumber === entry.target.id) avancement.style.height = '100%';
        });
      }
    });
  });
  steps.forEach((step) => { observer.observe(step); });
};

export default stepsScrollInit;
