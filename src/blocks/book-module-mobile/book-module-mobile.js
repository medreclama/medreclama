import Cookies from 'js-cookie';

// eslint-disable-next-line consistent-return
const bookModuleMobile = () => {
  const bookModule = document.getElementById('book-module-mobile'); // модуль книги

  if (!bookModule) {
    return false;
  }

  const bookModuleOpener = bookModule.querySelector('.book-module-mobile__open'); // кнопка открытия модуля
  const bookModuleCloser = bookModule.querySelector('.book-module-mobile__close'); // кнопка закрытия модуля
  const bookModuleForm = bookModule.querySelector('.book-module-mobile__form'); // форма в модуле
  const bookModuleTitle = bookModule.querySelector('.book-module-mobile__title'); // форма в модуле
  const script = document.createElement('script');
  script.src = 'https://api.siter.justclick.ru/web_forms/5ec687899fe1864f9d000262/form_loader.js?cid=form-5ec687899fe1864f9d000262';
  script.id = 'form-5ec687899fe1864f9d000262';
  const userEvents = () => {
    window.removeEventListener('scroll', userEvents);
    window.removeEventListener('mousemove', userEvents);
    bookModuleTitle.append(script);
  };
  window.addEventListener('scroll', userEvents);
  window.addEventListener('mousemove', userEvents);

  let openerWasActive = false;

  // открытие модуля книги
  bookModuleOpener.addEventListener('click', () => {
    bookModule.classList.toggle('book-module-mobile--active');
    bookModuleOpener.classList.remove('book-module-mobile__open--active');
    openerWasActive = true;
    return openerWasActive;
  });

  // закрытие модуля книги при клике на крестик
  bookModuleCloser.addEventListener('click', () => {
    bookModule.classList.remove('book-module-mobile--active');
  });

  // закрытие модуля книги при клике вне модуля
  document.body.addEventListener('click', (e) => {
    let { target } = e;
    while (target !== document.body) {
      if (target === bookModule) {
        return;
      }
      target = target.parentNode;
    }
    if (target === document.body && bookModule.classList.contains('book-module-mobile--active')) {
      bookModule.classList.remove('book-module-mobile--active');
    }
  });

  // заглушка для функции сабмита
  if (bookModuleForm) {
    bookModuleForm.addEventListener('submit', (event) => {
      event.preventDefault();

      const bookModuleName = bookModuleForm.querySelector('.form-input[type="text"]');
      const bookModuleEmail = bookModuleForm.querySelector('.form-input[type="email"]');

      Cookies.set('sendBook_name', bookModuleName.value, { expires: 30 });
      Cookies.set('sendBook_email', bookModuleEmail.value, { expires: 30 });
    });
  }

  document.addEventListener('DOMContentLoaded', () => {
    let isAutoShowed = false;
    if (Cookies.get('isAutoShowed') !== undefined) {
      isAutoShowed = true;
    }

    setTimeout(() => {
      if (!openerWasActive && !isAutoShowed) {
        bookModuleOpener.classList.add('book-module-mobile__open--active');
        openerWasActive = true;
        Cookies.set('isAutoShowed', true);
      }
    }, 7000);
  });
};
export default bookModuleMobile;
