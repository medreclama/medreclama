const youtubeFrames = document.querySelectorAll('.youtube-responsive__iframe');
const youtubeLoadingLazy = () => {
  youtubeFrames.forEach((frame) => {
    if (!frame.dataset.src) return;
    const userEvents = () => {
      window.removeEventListener('scroll', userEvents);
      window.removeEventListener('mousemove', userEvents);
      // eslint-disable-next-line no-param-reassign
      frame.querySelector('iframe').setAttribute('src', `https://www.youtube.com/embed/${frame.dataset.src}`);
    };
    window.addEventListener('scroll', userEvents);
    window.addEventListener('mousemove', userEvents);
  });
};

export default youtubeLoadingLazy;
