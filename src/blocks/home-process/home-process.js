const homeProcess = () => {
  const items = document.querySelectorAll('.home-process__item');
  if (!items) return;
  items.forEach((item) => {
    item.addEventListener('click', (event) => {
      const arrow = item.querySelector('.home-process__arrow');
      const arrowAnimateOpen = arrow.querySelector('#open');
      const arrowAnimateClose = arrow.querySelector('#close');
      if (event.target.closest('.home-process__title')) item.classList.toggle('home-process__item--active');
      if (item.classList.contains('home-process__item--active')) arrowAnimateOpen.beginElement();
      else arrowAnimateClose.beginElement();
    });
  });
};

export default homeProcess;
